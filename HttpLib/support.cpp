#include "support.h"

/**********************************************************************************************//**
 * \fn	void log_reader(const char* tag, const char* log)
 *
 * \brief	Logs a reader
 *
 * \author	Zeder
 * \date	6/25/2020
 *
 * \param 	tag	The tag.
 * \param 	log	The log.
 **************************************************************************************************/

void log_reader(const char* tag, const char* log)
{
	uint8_t res;
	time_t time_ = time(nullptr);
	struct tm* tm = localtime(&time_);
	FILE* log_running  = fopen("/root/kiin/log_running.txt", "a+");
	if (nullptr == log)
	{
		printf("failed open log \n");
		
	}
	fprintf(log_running, "%s: %s \n", tag, log);
	fclose(log_running);
}
uint8_t copy_f2f(const char *source, const char *dest)
{
	int fdSource = open(source, O_RDWR);

   /* Caf's comment about race condition... */
   if (fdSource > 0){
     if (lockf(fdSource, F_LOCK, 0) == -1) return 0; /* FAILURE */
   }
	else return FAILED; /* FAILURE */

   /* Now the fdSource is locked */

   int fdDest = open(dest, O_CREAT);
   off_t lCount;
   struct stat sourceStat;
   if (fdSource > 0 && fdDest > 0){
      if (!stat(source, &sourceStat)){
          int len = sendfile(fdDest, fdSource, &lCount, sourceStat.st_size);
          if (len > 0 && len == sourceStat.st_size){
               close(fdDest);
               close(fdSource);

               /* Sanity Check for Lock, if this is locked -1 is returned! */
               if (lockf(fdSource, F_TEST, 0) == 0){
                   if (lockf(fdSource, F_ULOCK, 0) == -1){
                      /* WHOOPS! WTF! FAILURE TO UNLOCK! */
                   }else{
                      return SUCCESS; /* Success */
                   }
               }else{
                   /* WHOOPS! WTF! TEST LOCK IS -1 WTF! */
                   return FAILED; /* FAILURE */
               }
          }
      }
   }
   return 0; /* Failure */
}



void get_time(tm** tm)
{
	time_t time_ = time(nullptr);
	
//	std::cout << time_ << std::endl;
	*tm = localtime(&time_);
}
time_t restrict_time(std::string dataRestrict)
{   
	struct tm* reverse_tm;
	std::cout << dataRestrict << '\n';
	char temp_str[100];
	sprintf(temp_str, "%s", dataRestrict.c_str());
	
	int year,mon,day,hour,min,sec;
	sscanf(temp_str, "%d-%d-%d %d:%d:%d", &year, &mon, &day, &hour, &min, &sec);
	
	//	sscanf(temp_str, "%d-%d-%d %d:%d:%d", &reverse_tm->tm_year, &reverse_tm->tm_mon, &reverse_tm->tm_mday, &reverse_tm->tm_hour, &reverse_tm->tm_min, &reverse_tm->tm_sec);
	//	reverse_tm.tm_year = year;
	//	reverse_tm.tm_mon = mon;
	//	reverse_tm.tm_mday = day;
	//	reverse_tm.tm_hour = hour;
	//	reverse_tm.tm_min = min;
	//	reverse_tm.tm_sec = sec;
		reverse_tm = getdate(dataRestrict.c_str());
	
	return mktime(reverse_tm);
}
