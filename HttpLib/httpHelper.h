#ifndef _HTTPHELPER_H
#define _HTTPHELPER_H
#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include "time.h"

//#define DEVICE_ID "00000050"
#define DEVICE_ID "00100001"
struct MemoryStruct {
    char* memory;
    size_t size;
    uint16_t res_code;
};


#ifdef __cplusplus
extern "C" {
#endif


    typedef uint8_t code;
    int CurlTest(void);

	uint16_t device_alive(char* deviceID);
    MemoryStruct get_whitelist(char* deviceID);

    MemoryStruct get_whitelist_v2(char* deviceID,bool today);
    uint16_t post_attendance(char* body);

    //MCP API v2
    MemoryStruct device_alive_ver2(char* deviceID);
	uint16_t delete_notification(const char* deviceID, char* date);


#ifdef __cplusplus
}
#endif

#endif
