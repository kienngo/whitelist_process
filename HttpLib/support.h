#ifndef SUPPORT_H_
#define SUPPORT_H_

#include <iostream>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <cstdint>
#include <string.h>
#include <stdio.h>
#include <ctime>
#ifdef __cplusplus
extern "C" {
#endif
	typedef enum
	{
		FAILED    = 0,
		SUCCESS
	} support_code;
	
	uint8_t copy_f2f(const char *source, const char *dest);
	void log_reader(const char* tag,const char* log);
	
	void get_time(struct tm** tm);
	time_t restrict_time(std::string dataRestrict);
#ifdef __cplusplus
}
#endif

#endif