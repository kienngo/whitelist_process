#include "api.h"
#include <thread>
#include <stdio.h>
#include <stdint.h>
#include <future>
#include <algorithm>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/range/algorithm/set_algorithm.hpp>
#include <mutex>
#include <cstdint>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

using namespace std;

//namespace pt = boost::property_tree;

std::mutex _mutex;

//std::string root_whitelist_dir = "/root/CnsReader/whitelist/whitelist_";
std::string root_whitelist_dir = "/home/CnsReader/whitelist/";
std::string root_whitelist_dir_tomorrow = "/home/CnsReader/whitelist/Tomorrow/";
//std::string root_eventId_dir = "/root/CnsReader/event/event_";
std::string root_eventId_dir = "/home/CnsReader/event/";
std::string root_eventId_dir_tomorrow = "/home/CnsReader/event/Tomorrow/";
uint8_t nWhitelist_enable = 1;
extern configMachine config;


void virtual_data()
{
	//std::fstream fs;
	//fs.open("/home/CnsReader/Config/Virtual.txt", std::fstream::out | std::fstream::in | std::fstream::app);
	//if (fs.is_open())
	//{
	//	char data[50];
	//	std::string virData;
	//	for (auto i = 0; i < 5000; i++)
	//	{								  
	//		virData.append("11110000000000");
	//		sprintf(data, "05%d", i);
	//		virData.append(std::string(data));
	//		fs << "\"" << virData << "\"," << '\n';	
	//		virData.clear();
	//	}
	//}
	//fs.close();
}

template <class T, size_t N>
T sum(T(&array)[N]) {
	return std::accumulate(std::begin(array), std::end(array), T());
}

//template <class T, size_t N>
//eventId_t ProcessArEventID(T(&newEventId)[N], T(&oldEventId)[N])
//template <class T, size_t N>
//************************************
// Method:    ProcessArEventID
// FullName:  ProcessArEventID
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: eventId_t * tEventId
//************************************
void ProcessArEventID(eventId_t* tEventId)
{
	/*oldEventId[] = [1,2,3,4]
	*newEventId[] = [3,4,5,6]
	*AddEventId = [5,6]
	*UpdateEventId = [3,4]
	*DeleteEventId = [1,2]
	*/

	//Get Add eventID
	uint8_t index = 0;
	std::vector<int> cv;
	std::set_difference(std::begin(tEventId->newEventId),
		std::begin(tEventId->newEventId) + tEventId->newEventAmount,
		std::begin(tEventId->oldEventId),
		std::begin(tEventId->oldEventId) + tEventId->oldEventAmount,
		std::back_inserter(cv));

	std::cout << "AddEventId" << '\n';
	for (auto& s : cv)
	{
		std::cout << s << " ";
		tEventId->addEventId[index++] = s;
	}
	std::cout << '\n';
	tEventId->addEventIdLength = index;

	//Get update eventID
	index = 0;
	std::vector<int> cvi;
	std::set_intersection(std::begin(tEventId->newEventId),
		std::begin(tEventId->newEventId) + tEventId->newEventAmount,
		std::begin(tEventId->oldEventId),
		std::begin(tEventId->oldEventId) + tEventId->oldEventAmount,
		std::back_inserter(cvi));
	std::cout << "UpdateEventId" << '\n';
	for (auto& s : cvi)
	{
		std::cout << s << " ";
		tEventId->updateEventId[index++] = s;
	}

	std::cout << '\n';
	tEventId->updateEventIdLength = index;


	//Get delete eventID
	index = 0;
	std::vector<int> cvb;
	std::set_difference(std::begin(tEventId->oldEventId),
		std::begin(tEventId->oldEventId) + tEventId->oldEventAmount,
		std::begin(tEventId->newEventId),
		std::begin(tEventId->newEventId) + tEventId->newEventAmount,
		std::back_inserter(cvb));
	std::cout << "DeleteEventId" << '\n';
	for (auto& s : cvb)
	{
		std::cout << s << " ";
		tEventId->deleteEventId[index++] = s;
	}
	std::cout << '\n';
	tEventId->deleteEventIdLength = index;

}
//************************************
// Method:    compareWhitelistCard
// FullName:  compareWhitelistCard
// Access:    public 
// Returns:   UpdateContent_t
// Qualifier:
// Parameter: Json::Value newWhitelistCard
// Parameter: Json::Value oldWhitelistCard
//************************************
UpdateContent_t compareWhitelistCard(Json::Value newWhitelistCard, Json::Value oldWhitelistCard)
{
	UpdateContent_t m_UpdateContents;

	//Get Add eventID
	std::set<std::string>::iterator it;
	std::set<std::string> newWl, oldWl, setAddWL, setDelteWl, setDifference;

	//	newWl.insert(tEventId->newEventContents[0]["whitelistCards"].asString());
	for (int i = 0; i < newWhitelistCard["whitelistCards"].size(); i++)
	{
		newWl.insert(newWhitelistCard["whitelistCards"][i].asString());

	}
	//	oldWl.insert(tEventId->oldEventContents[0]["whitelistCards"].asString());
	for (int i = 0; i < oldWhitelistCard["whitelistCards"].size(); i++)
	{
		oldWl.insert(oldWhitelistCard["whitelistCards"][i].asString());
	}
	//	cout << "addWL-------------------------" << endl;
	boost::set_difference(newWl,
		oldWl,
		std::inserter(m_UpdateContents.listAddEventID, m_UpdateContents.listAddEventID.begin()));
	//	for (it = m_UpdateContents.listAddEventID.begin(); it != m_UpdateContents.listAddEventID.end(); ++it)
	//	{
	//		cout << *it << endl;
	//	}
	//	cout << "deleteWl-------------------------" << endl;
	boost::set_difference(oldWl,
		newWl,
		std::inserter(m_UpdateContents.listDeleteEventID, m_UpdateContents.listDeleteEventID.begin()));
	//	for (it = m_UpdateContents.listDeleteEventID.begin(); it != m_UpdateContents.listDeleteEventID.end(); ++it)
	//	{
	//		cout << *it << endl;
	//	}
	return m_UpdateContents;
}
//************************************
// Method:    processUpdateEventIdContents
// FullName:  processUpdateEventIdContents
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: eventId_t * tEventId
//************************************
void processUpdateEventIdContents(eventId_t* tEventId)
{

	UpdateContent_t t_UpdateContents;
	for (int i = 0; i != tEventId->updateEventIdLength; i++)
	{
		int indexNew, indexOld;
		for (int j = 0; j != tEventId->newEventAmount; j++)
		{
			if (tEventId->updateEventId[i] == tEventId->newEventContents[j]["id"].asInt())
			{
				indexNew = j;
			}

		}
		for (int j = 0; j != tEventId->oldEventAmount; j++)
		{
			if (tEventId->updateEventId[i] == tEventId->oldEventContents[j]["id"].asInt())
			{
				indexOld = j;
			}

		}

		t_UpdateContents = compareWhitelistCard(tEventId->newEventContents[indexNew], tEventId->oldEventContents[indexOld]);
		t_UpdateContents.id = tEventId->newEventContents[i]["id"].asString();

		int m_index = 0;
		tEventId->updatelist_add[i]["id"] = t_UpdateContents.id;
		if (t_UpdateContents.listAddEventID.size() != 0)
		{
			for (auto& it : t_UpdateContents.listAddEventID)
			{

				tEventId->updatelist_add[i]["whitelistCards"][m_index++] = it;
			}
		}
		else tEventId->updatelist_add[i]["whitelistCards"] = Json::Value(Json::arrayValue);


		m_index = 0;
		tEventId->updatelist_delete[i]["id"] = t_UpdateContents.id;
		if (t_UpdateContents.listDeleteEventID.size() != 0)
		{
			for (auto& it : t_UpdateContents.listDeleteEventID)
			{

				tEventId->updatelist_delete[i]["whitelistCards"][m_index++] = it;
			}
		}
		else tEventId->updatelist_delete[i]["whitelistCards"] = Json::Value(Json::arrayValue);


		//		cout << "add whitelist" << endl; 
		//		cout << tEventId->updatelist_add[i] <<  endl;
		//		cout << "delete whitelist" << endl; 
		//		cout << tEventId->updatelist_delete[i] <<  endl; 
		//		tEventId->delete[i]["id"] = t_UpdateContents.id;
		//		tEventId->m_UpdateContents[i] = t_UpdateContents;
	}

	return;

}



//************************************
// Method:    DataAnalyst
// FullName:  DataAnalyst
// Access:    public 
// Returns:   eventId_t
// Qualifier:
// Parameter: Json::Value jNowBody
// Parameter: Json::Value jOldBody
// Parameter: bool today
//************************************
eventId_t DataAnalyst(Json::Value jNowBody, Json::Value jOldBody, bool today)
{
	eventId_t tEventId;

	tEventId.newEventAmount = jNowBody["events"].size();
	//Create Array new and old EventId
	cout << "New-----------------------------------------------------------" << '\n';
	for (uint8_t i = 0; i < tEventId.newEventAmount; i++)
	{
		tEventId.newEventId[i] = jNowBody["events"][i]["id"].asInt();
		tEventId.newEventContents[i]["id"] = tEventId.newEventId[i];
		tEventId.newEventContents[i]["whitelistCards"] = jNowBody["events"][i]["whiteListCards"];
		//		cout << tEventId.newEventContents[i] << endl;
	}
	tEventId.oldEventAmount = jOldBody["events"].size();
	cout << "Old-----------------------------------------------------------" << '\n';
	for (uint8_t i = 0; i < tEventId.oldEventAmount; i++)
	{
		tEventId.oldEventId[i] = jOldBody["events"][i]["id"].asInt();
		tEventId.oldEventContents[i]["id"] = tEventId.oldEventId[i];
		tEventId.oldEventContents[i]["whitelistCards"] = jOldBody["events"][i]["whiteListCards"];
		//		cout << tEventId.oldEventContents[i] << endl;
	}

	ProcessArEventID(&tEventId);
	processUpdateEventIdContents(&tEventId);

	//Prepare list add - update - delete
	fstream fs;
	if (0 != tEventId.addEventIdLength)
	{
		for (int i = 0; i < tEventId.addEventIdLength; i++)
		{
			for (uint8_t j = 0; j < tEventId.newEventAmount; j++)
			{
				if (tEventId.addEventId[i] == tEventId.newEventContents[j]["id"].asInt())
				{
					updateWhitelist(tEventId.newEventContents[j], today);
				}
			}
		}
	}
	if (0 != tEventId.updateEventIdLength)
	{
		for (int i = 0; i < tEventId.updateEventIdLength; i++)
		{
			updateWhitelist(tEventId.updatelist_add[i], today);
			deleteWhitelist(tEventId.updatelist_delete[i], today);




		}
	}
	if (0 != tEventId.deleteEventIdLength)
	{
		for (int i = 0; i < tEventId.deleteEventIdLength; i++)
		{
			//Find Index Event delete
			for (uint8_t j = 0; j < tEventId.oldEventAmount; j++)
			{
				cout << "updateEventId: " << tEventId.deleteEventId[i] << " compare " << "newEventContents " << tEventId.oldEventContents[j]["id"].asInt() << '\n';
				if (tEventId.deleteEventId[i] == tEventId.oldEventContents[j]["id"].asInt())
				{
					deleteWhitelist(tEventId.oldEventContents[j], today);
				}
			}

		}
	}
	return tEventId;
}

//
//Search in Json
//
//
std::vector<Json::Value> SearchInArray(const Json::Value& json, const std::string& key, const std::string& value)
{
	assert(json.isArray());

	std::vector<Json::Value> results;
	for (int i = 0; i != json.size(); i++)
		if (json[i][key].asString() == value)
			results.push_back(json[i]);
	return results;
}

//
//Check event with time form start-stop time and return event
//
uint8_t time_check(std::string Time, std::string startTime, std::string stopTime)
{
	//Format: Hour:Min:Sec
	//
	//
	int TimeHour, TimeMin;
	int startHour, startMin;
	int stopHour, stopMin;
	stringstream    sTime; sTime << Time;
	stringstream	sStartTime; sStartTime << startTime;
	stringstream	sStopTime; sStopTime << stopTime;

	//GetHour
	sTime >> TimeHour;
	sTime.get();
	//GetMin
	sTime >> TimeMin;

	sStartTime >> startHour;
	sStartTime.get();
	sStartTime >> startMin;


	sStopTime >> stopHour;
	sStopTime.get();
	sStopTime >> stopMin;
	if (startHour == stopHour)
	{
		if (!((TimeMin >= startMin) && (TimeMin <= stopMin)))	return 0;
	}
	else if (!((TimeHour >= startHour) && (TimeHour <= stopHour))) return 0;
	return 1;

}
//
//Utils function
//Get contents of json files
//P
//
Json::Value getJsonContens(std::string fileDir)
{
	Json::Value jSonValue;
	std::fstream fs;
	if (FileExists(fileDir.c_str()))
	{
		fs.open(fileDir, std::fstream::in);
		if (fs.is_open())
		{
			Json::Reader reader;
			//fs >> jSonValue;
			reader.parse(fs, jSonValue);
		}
		fs.close();
		return jSonValue;
	}
	return jSonValue;
}

//
//FindEvent
//Param: Times. Format "HH-MM"
//Return: events available in this times
//

std::vector<std::string> FindEvent(std::string Times)
{
	std::vector<std::string> result;
	std::fstream fs;
	std::string eventId_dir;
	eventId_dir.append(root_eventId_dir);
	eventId_dir.append("event");
	eventId_dir.append(".txt");

	std::cout << "eventId_dir: " << eventId_dir << '\n';
	std::string eventID;
	std::string event_start_time;
	std::string event_stop_time;
	if (FileExists(eventId_dir.c_str()))
	{

		Json::Value eventId_value;
		eventId_value = getJsonContens(eventId_dir);

		//cout << eventId_value << '\n';	
		if (eventId_value.isNull()) return vector<string>();


		int numofEvent = eventId_value["events"].size();
		for (int i = 0; i < numofEvent; i++)
		{
			eventID = eventId_value["events"][i]["id"].asString();
			event_start_time = eventId_value["events"][i]["time"]["startDateTime"].asString();
			event_stop_time = eventId_value["events"][i]["time"]["endDateTime"].asString();

			event_start_time = event_start_time.substr(event_start_time.find_last_of(" "), event_start_time.size());
			event_stop_time = event_stop_time.substr(event_stop_time.find_last_of(" "), event_stop_time.size());

			//
			//time_check: check eventId in event_start_time to event_stop_time
			//
			if (time_check(Times, event_start_time, event_stop_time))
			{
				cout << "Event " << eventID << '\n';
				result.push_back(eventID);
			}
		}
	}
	return result;
}
void WriteEvent(Json::Value jNowBody, bool today)
{

	std::string event_id;
	bool accessControlOnly;
	std::string date;
	std::string event_start_time;
	std::string event_stop_time;
	std::string eventId_dir, eventId_name;

	if (today)
	{
		eventId_dir.append(root_eventId_dir);
		eventId_name.append(eventId_dir);
		eventId_name.append("event");
		eventId_name.append(".txt");

	}
	else {
		eventId_dir.append(root_eventId_dir_tomorrow);
		eventId_name.append(eventId_dir);
		eventId_name.append("event");
		eventId_name.append(".txt");
	}


	std::cout << "eventId_dir: " << eventId_name << '\n';


	fstream fs;

	auto jNowAmountEvent = jNowBody["events"].size();

	if (FileExists(eventId_name.c_str()))
	{

		std::cout << "eventId file available" << '\n';
		fs.open(eventId_name, std::fstream::out | std::fstream::in | std::fstream::trunc);
		if (fs.is_open())
		{
			//			std::cout << "open " << eventId_name << std::endl;
			std::cout << "WriteEvent: eventId_name " << eventId_name << '\n';
			Json::Value eventId_value;

			{
				auto t = std::time(nullptr);
				auto tm = *std::localtime(&t);

				std::ostringstream oss;
				oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
				date = oss.str();
				eventId_value["date"] = date;
			}

			if (0 != jNowAmountEvent)
			{

				for (uint8_t i = 0; i < jNowAmountEvent; i++)
				{

					event_id = jNowBody["events"][i]["id"].asString();
					event_start_time = jNowBody["events"][i]["startDateTime"].asString();
					event_stop_time = jNowBody["events"][i]["endDateTime"].asString();
					accessControlOnly = jNowBody["events"][i]["accessControlOnly"].asBool();

					eventId_value["events"][i]["id"] = event_id;
					eventId_value["events"][i]["accessControlOnly"] = accessControlOnly;
					eventId_value["events"][i]["time"]["endDateTime"] = event_stop_time;
					eventId_value["events"][i]["time"]["startDateTime"] = event_start_time;


				}
				Json::StreamWriterBuilder builder;
				std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
				writer->write(eventId_value, &fs);
				//fs << eventId_value << '\n';
			}
			else
			{
				fs << "{}";
				fs.close();
				return;
			}
			//			std::cout << eventId_value << std::endl;

		}
		else {
			std::cout << "openEventFailed" << '\n';
		}
		fs.close();
	}
	else
	{


		std::cout << "eventId file not exist" << '\n';
		fs.open(eventId_name, std::fstream::app | std::fstream::in);
		if (fs.is_open())
		{
			std::cout << "WriteEvent eventId_name " << eventId_name << '\n';
			Json::Value eventId_value;

			//
			//last time get whitelist
			//Write "date":
			//
			{
				auto t = std::time(nullptr);
				auto tm = *std::localtime(&t);

				std::ostringstream oss;
				oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
				date = oss.str();
				eventId_value["date"] = date;
			}

			for (uint8_t i = 0; i < jNowAmountEvent; i++)
			{
				event_id = jNowBody["events"][i]["id"].asString();
				event_start_time = jNowBody["events"][i]["startDateTime"].asString();
				event_stop_time = jNowBody["events"][i]["endDateTime"].asString();

				eventId_value["events"][i]["id"] = event_id;
				eventId_value["events"][i]["time"]["endDateTime"] = event_stop_time;
				eventId_value["events"][i]["time"]["startDateTime"] = event_start_time;

			}
			fs << eventId_value << '\n';
		}
		fs.close();
	}
}
/**********************************************************************************************//**
 * \fn	void* whitelist(void* arg)
 *
 * \brief	Whitelist function
 *
 * \author	Zeder
 * \date	9/24/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **/
uint8_t whitelist_api(bool today)
{
	//	std::unique_lock<std::mutex> uniqLock(_mutex, std::defer_lock);

	MemoryStruct white_list;


	struct tm* tm;
	get_time(&tm);

	//
	//use whitelist v2 to get new white list
	//rescode = 200 is successful
	//
	//
	white_list = get_whitelist_v2((char*)config.config_.mcpConfig_.terminalId.c_str(), today);
	if (200 == white_list.res_code)
	{
		//Process body of request

		Json::Value  vOldWhitelist, vCurrentWhitelist;
		Json::Reader jSonReader;

		//		std::cout  << white_list.memory << std::endl;

		std::fstream rawLog;
		//
		//TODO: WhitelistRaw Log location
		//
		std::string rawWl_dir;
		if (today) {
			rawWl_dir = "/home/CnsReader/whitelist/whitelistLog.txt";
		}
		else rawWl_dir = "/home/CnsReader/whitelist/Tomorrow/whitelistLog.txt";

		//
		//Write body raw into the file .txt for stored history
		//
		//
		rawLog.open(rawWl_dir, std::fstream::out | std::fstream::in);

		//
		//Two case: rawLog is exist or not exist
		//
		if (rawLog.is_open())
		{
			//
			//rawLog exist
			//rawLog contents will be storage in the vOldWhitelist
			//Purpose: compare with vCurrentWhitelist
			//

			//Get rawLog old -> vOldWhitelist 
			rawLog >> vOldWhitelist;
			rawLog.close();
			rawLog.open(rawWl_dir, std::fstream::out | std::fstream::in | std::fstream::trunc);
			if (rawLog.is_open())
			{
				//Write Current data to file
				rawLog << white_list.memory;
				rawLog.close();
			}

		}
		else
		{
			//
			//rawLog isnt exist.
			// -> vOldWhitelist == null
			//
			rawLog.open(rawWl_dir, std::fstream::out | std::fstream::in | std::fstream::trunc);
			if (rawLog.is_open())
			{
				rawLog << white_list.memory;
				rawLog.close();
			}
		}

		//
		//Storage the body of request -> vCurrentWhitelist
		//
		jSonReader.parse(white_list.memory, vCurrentWhitelist);


		//
		//Check
		//
		if (vCurrentWhitelist["success"].asString() != "true")
		{
			std::cout << "get whitelist fail" << '\n';
			//return 0;
			return 0;
		}
		//
		//Here we have vCurrentWhitelist and vOldWhitelist
		//

		//
		//Begin write event file
		//
		eventId_t tEventId;
		WriteEvent(vCurrentWhitelist, today);
		tEventId = DataAnalyst(vCurrentWhitelist, vOldWhitelist, today);
		//		WriteEvent(vCurrentWhitelist, today);
		//		tEventId = DataAnalyst(vCurrentWhitelist, Json::Value(), today);

				//TEST
				//eventId_t tEventId = DataAnalyst(vOldWhitelist, vCurrentWhitelist);
				//

				//TODO
		return 1;
	}

	return 0;
}

//
//Current: Dont use-> Dont care
//Prepare for future:
//Function: find card in white list
//
uint8_t whitelist_check(char* card_number) {
	std::string whitelist_dir, whitelist_file;
	uint8_t result;

	std::fstream fs;

	struct tm* tm;
	get_time(&tm);
	char time_create[50];
	sprintf(time_create, "%04d%02d%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday);


	whitelist_dir.clear();
	whitelist_dir.append(root_whitelist_dir);
	whitelist_dir.append(std::string(time_create));
	whitelist_file.clear();
	whitelist_file.append(whitelist_dir);
	whitelist_file.append("/");
	whitelist_file.append(std::string(card_number));
	whitelist_file.append(".txt");
	std::cout << "check whitelist file: " << whitelist_file << '\n';
	if (FileExists(whitelist_file.c_str()))
	{
		//Checktime
		Json::Value whitelist_value;
		fs.open(whitelist_file, std::fstream::out | std::fstream::in);
		if (fs.is_open())
		{

			fs >> whitelist_value;
			std::cout << "whitelist_value: " << whitelist_value << '\n';
			for (auto a = 0; a < whitelist_value["eventid"].size(); a++)
			{
				std::cout << "eventId: " << whitelist_value["eventid"][a].asString() << '\n';
			}

			fs.close();
		}
		result = 0;
	}
	else result = 1;
	return result;
}


/**********************************************************************************************//**
 * \fn	void keep_alive()
 *
 * \brief	Keep alive
 *
 * \author	Zeder
 * \date	9/29/2020
 *
 * \param [in,out]	arg	If non-null, the argument.
 *
 * \returns	Null if it fails, else a pointer to a void.
 **************************************************************************************************/

void keep_alive()
{
	uint32_t time_interval = 0;
	unsigned int now;
	uint8_t res = 0;


	MemoryStruct response_body;

	printf("keep alive in thread\n");
	for (;;)
	{

		printf("keep alive in process\n");

		//
		//Use keep_alive version 2 to check the white list changed?
		//
		response_body = device_alive_ver2((char*)config.config_.mcpConfig_.terminalId.c_str());

		//
		//If res_code = 200. Request keep_alive ok
		//Do check keywork ["changes"] 
		//
		if (200 == response_body.res_code)
		{
			//std::cout << response_body.memory << std::endl;
			if (response_body.size != 0)
			{

				Json::Value  jBody;
				Json::Reader jSonReader;

				bool parsingSuccessful = jSonReader.parse(response_body.memory, jBody);
				if (!parsingSuccessful)
				{
					std::cout << "Error parsing the string\n";
					break;
				}

				std::cout << jBody << '\n';
				uint8_t nLengthChanges = jBody["changes"].size();

				//
				//Check keywork  "changes" before do something
				//
				if (0 == jBody["changes"].size()) {
					//
					//Dont change
					//Skip this
					//
					cout << "dont thing change " << '\n';

				}
				else {
					std::cout << nLengthChanges << '\n';
					struct tm* tm;
					get_time(&tm);
					//TODO: Call api whitelist
					whitelist_api(true);


					get_time(&tm);
					char time_create[50];
					sprintf(time_create, "%04d-%02d-%02d %02d:%02d:%02d", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min + 1, tm->tm_sec);
					res = delete_notification(config.sDeviceID.c_str(), time_create);
					if (200 == res)
					{
						cout << "detele notification ok" << '\n';
					}
				}
			}
		}
		cout << response_body.res_code << '\n';
		sleep(60);
	}
}
bool FileExists(const char* filename)
{
	return std::filesystem::exists(filename);
	//struct stat fileInfo;
	//return stat(filename, &fileInfo) == 0;
}

//Require:  whitelistFileDir exists
//0: success 1: fail
uint8_t whitelistFile_process(std::string whitelistFileName, std::string eventId, std::string process)
{

	fstream fs;
	std::string whitelistFileDir;

	whitelistFileDir.append(whitelistFileName);

	Json::Value jsWhitelistContent;
	//	cout << "whitelist Dir " << whitelistFileDir << endl;
//		cout << "process eventId " <<  eventId << endl;
	fs.open(whitelistFileDir, fstream::in | fstream::out);
	if (fs.is_open())
	{
		fs >> jsWhitelistContent;
		fs.close();
	}
	else return 1;

	//Check eventId have yet in whitelist file
	auto eventId_exists = 0;
	auto eventId_index = 0;
	for (auto index = 0; index < jsWhitelistContent["eventid"].size(); index++)
	{
		if (eventId == jsWhitelistContent["eventid"][index].asString()) {
			//			std::cout << " eventId exist in whitelist contents" << std::endl;
			eventId_exists++;
			eventId_index = index;
			break;
		}
	}
	//If eventID haven't in whitelist. Add eventId at the end of array
	if (0 == eventId_exists)
	{
		//eventId haven't in value["eventid"]
		if (process == "add")
		{
			cout << "add process " << '\n';
			cout << "before :" << jsWhitelistContent["eventid"] << '\n';
			jsWhitelistContent["eventid"][jsWhitelistContent["eventid"].size()] = eventId;
			cout << "after: " << jsWhitelistContent["eventid"] << '\n';
		}
		else
		{
			cout << "not have " << process << "command " << '\n';
		}
	}
	else
	{
		if (process == "delete")
		{
			cout << "delete process \n";
			cout << "before :" << jsWhitelistContent["eventid"] << '\n';
			jsWhitelistContent["eventid"].removeIndex(0, &jsWhitelistContent["eventid"][eventId_index]);
			cout << "after: " << jsWhitelistContent["eventid"] << '\n';
		}
		eventId_exists = 0;
	}
	//TODO: Rewrite json -> file. Check performance here
	fs.open(whitelistFileDir, std::fstream::out | std::fstream::in | std::fstream::trunc);
	if (fs.is_open())
	{
		fs << jsWhitelistContent;
	}
	fs.close();

	return 0;
}
uint8_t add_eventId(std::string whitelistFileName, std::string eventId, bool today)
{
	fstream fs;
	std::string whitelistFileDir;
	//"/home/CnsReader/whitelist/";
	if (today)
	{
		whitelistFileDir.append(root_whitelist_dir);
		whitelistFileDir.append(whitelistFileName);
		whitelistFileDir.append(".txt");
	}
	else {
		whitelistFileDir.append(root_whitelist_dir_tomorrow);
		whitelistFileDir.append(whitelistFileName);
		whitelistFileDir.append(".txt");
	}


	Json::Value jsWhitelistContent;
	fs.open(whitelistFileDir, fstream::in | fstream::out);
	if (fs.is_open())
	{
		fs >> jsWhitelistContent;
		fs.close();
	}

	//Check eventId have yet in whitelist file
	auto eventId_exists = 0;
	for (auto lengthEventId = 0; lengthEventId < jsWhitelistContent["eventid"].size(); lengthEventId++)
	{
		if (eventId == jsWhitelistContent["eventid"][lengthEventId].asString()) {
			cout << "eventId exist in whitelist contents" << '\n';
			eventId_exists++;
			break;
		}
	}
	//If eventID haven't in whitelist. Add eventId at the end of array
	if (0 == eventId_exists)
	{
		//eventId haven't in value["eventid"]
		jsWhitelistContent["eventid"][jsWhitelistContent["eventid"].size()] = eventId;
	}
	else
	{
		//eventId exists in value["eventid"]
		//Don't
		eventId_exists = 0;
	}
	//TODO: Rewrite json -> file. Check performance here
	fs.open(whitelistFileDir, std::fstream::out | std::fstream::in | std::fstream::trunc);
	if (fs.is_open())
	{
		fs << jsWhitelistContent;
	}
	fs.close();
	return 1;
}
uint8_t remove_eventId(std::string whitelistFileDir, std::string eventId)
{
	return 0;
}
void get_time(tm** tm)
{
	time_t time_ = time(nullptr);

	//	std::cout << time_ << std::endl;
	*tm = localtime(&time_);
}



uint8_t updateWhitelist(Json::Value newEventContents, bool today)
{
	std::string whitelist_dir, whitelist_name;
	fstream fs;
	auto nWhitelistLength = newEventContents["whitelistCards"].size();

	for (auto j = 0; j < nWhitelistLength; j++)
	{
		std::string cardnumber = newEventContents["whitelistCards"][j].asString();
		//TODO: whitelist dir 
		if (today) {
			whitelist_dir.clear();
			//whitelist_dir.append("/root/CnsReader/whitelist/whitelist_");
			whitelist_dir.append(root_whitelist_dir);
			whitelist_name.append(whitelist_dir);
			whitelist_name.append(cardnumber);
			whitelist_name.append(".txt");
			std::cout << whitelist_name << '\n';
		}
		else {
			whitelist_dir.clear();
			whitelist_dir.append(root_whitelist_dir_tomorrow);
			whitelist_name.append(whitelist_dir);
			whitelist_name.append(cardnumber);
			whitelist_name.append(".txt");
			std::cout << whitelist_name << '\n';
		}


		//TODO: write whitelist contents
		if (FileExists(whitelist_name.c_str()))
		{
			//						Json::Value  whitelist_value_exists;
			std::cout << "whitelist file exists " << std::endl;
			whitelistFile_process(whitelist_name, newEventContents["id"].asString(), "add");
		}
		else
		{
			Json::Value whitelist_value;
			fs.open(whitelist_name, std::fstream::in | std::fstream::app);
			if (fs.is_open())
			{
				whitelist_value["cardnumber"] = cardnumber;
				whitelist_value["eventid"][0] = newEventContents["id"].asString();
				fs << whitelist_value << '\n';
			}
			fs.close();
		}
		whitelist_name.clear();
	}
	return 0;
}


uint8_t deleteWhitelist(Json::Value oldEventContents, bool today)
{
	std::string whitelist_dir, whitelist_name;
	fstream fs;
	auto nWhitelistLength = oldEventContents["whitelistCards"].size();

	for (auto j = 0; j < nWhitelistLength; j++)
	{
		std::string cardnumber = oldEventContents["whitelistCards"][j].asString();
		//TODO: whitelist dir 
		if (today) {
			whitelist_dir.clear();
			//whitelist_dir.append("/root/CnsReader/whitelist/whitelist_");
			whitelist_dir.append(root_whitelist_dir);
			whitelist_name.append(whitelist_dir);
			whitelist_name.append(cardnumber);
			whitelist_name.append(".txt");
			std::cout << whitelist_name << '\n';
		}
		else {
			whitelist_dir.clear();
			whitelist_dir.append(root_whitelist_dir_tomorrow);
			whitelist_name.append(whitelist_dir);
			whitelist_name.append(cardnumber);
			whitelist_name.append(".txt");
			std::cout << whitelist_name << '\n';
		}

		//TODO: write whitelist contents
		if (FileExists(whitelist_name.c_str()))
		{
			//Json::Value  whitelist_value_exists;
//			std::cout << "whitelist file exists: " << oldEventContents["id"].asString() << std::endl;
			whitelistFile_process(whitelist_name, oldEventContents["id"].asString(), "delete");
		}
		whitelist_name.clear();
	}
	return 0;
}





bool  getWhitelistToday()
{
	std::string eventId_dir;
	eventId_dir.append(root_eventId_dir);
	eventId_dir.append("event");
	eventId_dir.append(".txt");

	//
	//Check /home/CnsReader/event/event.txt is exst
	//
	if (FileExists(eventId_dir.c_str()))
	{
		//
		//Check last time to get whitelist
		//
		Json::Value jsEventValue;
		jsEventValue = getJsonContens(eventId_dir);

		//
		//Json null -> return false
		//
		if (jsEventValue.isNull()) return false;

		//String format: Date-Month-Year, DD-MM-YYYY
		std::string date_in_eventFile = jsEventValue["date"].asString();

		stringstream    streamTime; streamTime << date_in_eventFile;
		int tDate, tMonth, tYear;
		//
		//Get first number in stringstream
		// Exam: 17-12-2020
		// => tDate = 17, tMonth = 12, tYear = 2020
		//
		streamTime >> tDate;
		streamTime.get();
		streamTime >> tMonth;
		streamTime.get();
		streamTime >> tYear;


		struct tm* tm;
		get_time(&tm);

		//
		//Before using
		//tm->tm_year + 1900,
		//tm->tm_mon + 1, 
		//tm->tm_mday, 
		//

		cout << "DD-MM-YY: " << tDate << " " << tMonth << " " << tYear << '\n';
		if ((tDate == tm->tm_mday) && (tMonth == tm->tm_mon + 1) && (tYear == tm->tm_year + 1900)) return true;
		return false;
	}
	else return false;
}
