
#ifndef _API_H
#define _API_H
#include <iostream>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <cstring>
#include <errno.h>
#include <curl/curl.h>
#include <httpHelper.h>
#include <set>

#include "pthread.h"
#include <bits/stdc++.h> 
#include <signal.h>
#include <jsoncpp/json/value.h>
#include <sys/time.h>
#include "configMachine.hpp"
#include <sys/stat.h>
//#include <json.hpp>
typedef struct
{
	std::set<std::string> listAddEventID;
	std::set<std::string> listDeleteEventID;
	std::string id;
}UpdateContent_t;

typedef struct
{
	int oldEventId[20];
	int newEventId[20];
	Json::Value newEventContents[20];
	Json::Value oldEventContents[20];
	uint8_t oldEventAmount;
	uint8_t newEventAmount;
	
	int addEventId[20];
	uint8_t addEventIdLength;
	int updateEventId[20];
	uint8_t updateEventIdLength;
	int deleteEventId[20];
	uint8_t deleteEventIdLength;
	
	Json::Value updatelist_add[20];
	Json::Value updatelist_delete[20];
	
	UpdateContent_t m_UpdateContents[20];
}eventId_t;


bool FileExists(const char* filename); 
uint8_t whitelistFile_process(std::string whitelistFileName, std::string eventId, std::string process);
uint8_t whitelist_api(bool today);
void keep_alive();

void get_time(tm** tm);

//template <class T, size_t N>
void ProcessArEventID(eventId_t *eventID);
Json::Value getJsonContens(std::string fileDir);
uint8_t add_eventId(std::string whitelistFileName, std::string eventId);
uint8_t remove_eventId(std::string whitelistFileDir, std::string eventId);
uint8_t updateWhitelist(Json::Value newEventContents, bool today);
uint8_t deleteWhitelist(Json::Value oldEventContents, bool today);
bool getWhitelistToday();
std::vector<std::string> FindEvent(std::string Times);
void WriteEvent(Json::Value jNowBody, bool today);
uint8_t time_check(std::string Time, std::string startTime, std::string stopTime);

#endif //_API_H